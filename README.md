# yawifi

Very simple tool to find, connect and disconnect the wifi on systems which have the ConnMan connection manager application. Particularly useful for Planet computer Gemini PDAs.