#!/usr/bin/env gbs3
'Some useful information on connmanctl here: https://www.mankier.com/1/connmanctl
'USE "gb.args"

Public Sub Main()
	ProcessArgs()
End

Private Sub ProcessArgs()
Dim arg, next_arg as String
Dim i as Integer

for each arg in ARGS
    'Print "Argument Nr. " & i & " = " & arg

	next_arg = ARGS[i + 1]
	
	if LCase(arg) = "-h" or LCase(arg) = "-help" then
		Print "Usage: connman.bas enable wifi"
		Print "Usage: connman.bas scan wifi"
		Print "Usage: connman.bas services"
		Print "Usage: connman.bas agent on"
		Print "Usage: connman.bas connect SSID"
	else if LCase(arg) = "enable" then
		'Let's see if they're asking for to enable wifi
        if LCase(next_arg) = "wifi"
            'They're asking to enable wifi, but it may alrady be on
            'We need to add a delay in here for a few seconds
            Print "Enabled wifi"
        end if
	else if LCase(arg) = "scan" then
		'Let's see if they're asking for a scan of wifi
		if LCase(next_arg) = "wifi"
			'They're asking for a scan of wifi
			'We need to add a delay in here for a few seconds
			Wait 1.8
			Print "Scan completed for wifi"
		end if
	else if LCase(arg) = "services" then
		'We are here because they've asked for services
		Print "*AO MyNetwork wifi_dc85de828967_68756773616d_managed_psk Other"
		Print "NET wifi_dc85de828967_38303944616e69656c73_managed_psk Another"
		Print "One wifi_dc85de828967_3257495245363836_managed_wep FourthNetwork"
		Print "wifi_dc85de828967_4d7572706879_managed_wep" 
		Print "AnO6penNetwork wifi_dc85de828967_4d6568657272696e_managed_none"
		Print "*AO MyNetwork wifi_dc85de828967_68756773616d_managed_psk Other"
		Print "NET wifi_dc85de828967_38303944616e69656c73_managed_psk Another"
		Print "One wifi_dc85de828967_3257495245363836_managed_wep FourthNetwork"
		Print "wifi_dc85de828967_4d7572706879_managed_wep" 
		Print "AnO6penNetwork wifi_dc85de828967_4d6568657272696e_managed_none"
		Print "*AO MyNetwork wifi_dc85de828967_68756773616d_managed_psk Other"
		Print "NET wifi_dc85de828967_38303944616e69656c73_managed_psk Another"
		Print "One wifi_dc85de828967_3257495245363836_managed_wep FourthNetwork"
		Print "wifi_dc85de828967_4d7572706879_managed_wep" 
		Print "AnO6penNetwork wifi_dc85de828967_4d6568657272696e_managed_none"
		Print "*AO MyNetwork wifi_dc85de828967_68756773616d_managed_psk Other"
		Print "NET wifi_dc85de828967_38303944616e69656c73_managed_psk Another"
		Print "One wifi_dc85de828967_3257495245363836_managed_wep FourthNetwork"
		Print "wifi_dc85de828967_4d7572706879_managed_wep" 
		Print "AnO6penNetwork wifi_dc85de828967_4d6568657272696e_managed_none"
	else if LCase(arg) = "connect" then
		'We are here because they're asking for a connection to a specific SSID
		if next_arg <> ""
			'They're asking for the wifi to be on
            'We need to add a delay in here for a few tenths of a second
            Wait 0.4
			'We have to hand the PASSPHRASE aspect at some point. Have to see how that works in practice with real connmanctl
            Print "Connected"
			Print next_arg
		end if


	else if LCase(arg) = "agent" then
		'We are here because they've asker for agent 
		if LCase(next_arg) = "on"
			'They're asking for the wifi to be on
            'We need to add a delay in here for a few tenths of a second
            Wait 0.4
            Print "Agent registered"
		end if
	end if
    Inc i
next

End

